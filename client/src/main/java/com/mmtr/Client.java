package com.mmtr;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmtr.gui.LoginWindow;
import com.mmtr.gui.ClientWindow;
import com.mmtr.gui.SplashWindow;
import com.mmtr.model.ActionCode;
import com.mmtr.model.ClientMessage;
import com.mmtr.model.ServerMessage;
import com.mmtr.model.Snake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static com.mmtr.model.ActionCode.EXIT;
import static com.mmtr.model.ActionCode.LOGIN;
import static com.mmtr.model.ActionCode.MOVE;
import static com.mmtr.model.ActionCode.NEW_GAME;
import static com.mmtr.model.ActionCode.PAUSE;
import static com.mmtr.model.Key.A;
import static com.mmtr.model.Key.D;
import static com.mmtr.model.Key.S;
import static com.mmtr.model.Key.W;
import static java.awt.Color.BLACK;
import static java.awt.Color.BLUE;
import static java.awt.Color.RED;
import static java.awt.Color.WHITE;
import static java.awt.Font.BOLD;
import static java.awt.Font.SERIF;
import static java.awt.Label.CENTER;
import static javax.swing.SpringLayout.SOUTH;
import static javax.swing.SpringLayout.NORTH;

public class Client extends JFrame implements Runnable{
	private Logger logger = LoggerFactory.getLogger(Client.class);
	private static final Long serialVersionUID = 1L;

	private static String host = "localhost";
	private static Integer port = 64444;
	private String name = "DefaultName";
	private final String CLIENT = "Client";

	private Socket socket;
	private BufferedReader bufferedReader;
	private PrintWriter printWriter;
	private ClientWindow clientWindow = new ClientWindow();
	private Clip clip;
	private JLabel score1;
	private JLabel score2;

	private static final ObjectMapper objectMapper = new ObjectMapper();

	public static void main(String[] args){
//		new SplashWindow("/images/splash.jpg");
		
		Client client = new Client();
		LoginWindow loginWindow = new LoginWindow(client, true);

		String name = loginWindow.getName();
		String host = loginWindow.getHost();
		Integer port = loginWindow.getPort();
		loginWindow.saveProperties();
		client.initGUI(host, port, name);
	}
	
	public Client() {
		addKeyListener(new KeyAdapter(){
			@Override
			public void keyPressed(KeyEvent e){
				ClientMessage clientMessage = new ClientMessage(name, MOVE);
				switch(e.getKeyChar()){
					case 'a':
						clientMessage.setKey(A);
						break;
					case 'w':
						clientMessage.setKey(W);
						break;
					case 'd':
						clientMessage.setKey(D);
						break;
					case 's':
						clientMessage.setKey(S);
						break;
					case ' ':
						clientMessage = new ClientMessage(name, PAUSE);
						break;
					case 'g':
						clientWindow.setShowGrid(clientWindow.getShowGrid() ? false : true);
						return;
				}

				sendJson(clientMessage);
			}
		});

		addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e){
				JOptionPane.showMessageDialog(null, "Client Close: " + name);
				close();
			}
		});
	}
	
	private void initGUI(String host, Integer port, String name){
		Client.host = host;
		Client.port = port;
		this.name = name;

		setTitle(CLIENT);
		JLabel label = new JLabel(CLIENT);
		label.setFont(new Font(SERIF, BOLD, 32));
		label.setOpaque(true);
		label.setForeground(WHITE);
		label.setBackground(BLACK);
		JPanel panel = new JPanel();
		panel.setBackground(BLACK);
		panel.add(label);
		add(panel, NORTH);
		add(clientWindow, CENTER);
		
		JPanel scorePanel = new JPanel();
		scorePanel.setLayout(new GridLayout(1, 2));
		score1 = new JLabel("Player1", SwingConstants.LEFT);
		score1.setFont(new Font(SERIF, BOLD, 16));
		score1.setOpaque(true);
		score1.setForeground(WHITE);
		score1.setBackground(RED);
		score2 = new JLabel("Player2", SwingConstants.RIGHT);
		score2.setFont(new Font(SERIF, BOLD, 16));
		score2.setOpaque(true);
		score2.setForeground(WHITE);
		score2.setBackground(BLUE);
		
		scorePanel.add(score1);
		scorePanel.add(score2);
		add(scorePanel, SOUTH);
		
		setResizable(false);
		pack();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setFocusable(true);	
		
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(this.getClass().getResource("/music/8-bit Detective.wav"));
			clip = AudioSystem.getClip();
			clip.open(audioInputStream);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		new Thread(this).start();
	}

	public void setScore(Snake snake){
		score1.setText(snake.getName() + ": " + snake.getBody().size());
		score2.setText("UNKNOWN" + ": " + 0);
	}
	
	public void run(){
		try{
			socket = new Socket(host, port);
			bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			printWriter = new PrintWriter(socket.getOutputStream(), true);
			
			ClientMessage clientMessage = new ClientMessage(name, LOGIN);
			sendJson(clientMessage);
			
			clip.loop(10);		
			
			boolean keepRunning = true;
			while(keepRunning){
				String input = bufferedReader.readLine();
				System.out.println("CLIENT READ: " + input);
				if(input == null){
					continue;
				}

				ServerMessage serverMessage = null;
				try{
					serverMessage = objectMapper.readValue(input, ServerMessage.class);
				} catch (Exception e){
					e.printStackTrace();
				}

				ActionCode actionCode = serverMessage.getActionCode();

				switch(actionCode){
					case PAUSE_ON:
						clientWindow.setPaused(true);
						clip.stop();
						break;
					case PAUSE_OFF:
						clip.start();
						clientWindow.setPaused(false);
						break;
					case SERVER_STOP:
						JOptionPane.showMessageDialog(this, "SERVER STOPPED");
						close();
						break;				
					case SERVER_UNAVAILABLE:
						JOptionPane.showMessageDialog(this, "SERVER UNAVAILABLE");
						close();
						break;
					case EXIT:
						JOptionPane.showMessageDialog(this, "See ya next time");
						System.exit(0);
						break;
					case REJECT:
						name = JOptionPane.showInputDialog(this, "Name " + name + " is not Available. Choose another one:");
						sendJson(new ClientMessage(name, LOGIN));
						break;
					case ACCEPT:
						System.out.println("Client accept: " + serverMessage);
						String name = serverMessage.getSnake().getName();
						setTitle("Player: " + name);
						this.name = name;
						clientWindow.setApple(serverMessage.getApple());
						clientWindow.setSnake(serverMessage.getSnake());
						break;
					case UPDATE:
						Snake snake = serverMessage.getSnake();
						setScore(snake);
						clientWindow.setApple(serverMessage.getApple());
						clientWindow.setSnake(snake);
						break;
					case LOSE:
						try{
							clip.stop();
							AudioInputStream aStrean = AudioSystem.getAudioInputStream(this.getClass().getResource("/music/loseSound.wav"));
							Clip sound = AudioSystem.getClip();
							sound.open(aStrean);
							sound.start();
							new SplashWindow("/images/lose.jpg");
							askForNewGame();
						}
						catch(Exception e){
							logger.error(e.getMessage());
						}
						break;
					case WIN:
						try{
							clip.stop();
							AudioInputStream audio = AudioSystem.getAudioInputStream(this.getClass().getResource("/music/winSound.wav"));
							Clip sound = AudioSystem.getClip();
							sound.open(audio);
							sound.start();
							new SplashWindow("/images/win.jpg");
							askForNewGame();
						}
						catch(Exception e){
							logger.error(e.getMessage());
						}
						break;
					case DRAW:
						try{
							clip.stop();
							AudioInputStream aStrean = AudioSystem.getAudioInputStream(this.getClass().getResource("/music/drawSound.wav"));
							Clip sound = AudioSystem.getClip();
							sound.open(aStrean);
							sound.start();
							new SplashWindow("/images/draw.jpg");
							askForNewGame();
						}
						catch(Exception e){
							logger.error(e.getMessage());
						}
						break;
					case RESTART:
						System.out.println("Client restart: " + serverMessage);
						clientWindow.setSnake(serverMessage.getSnake());
						clip.loop(5);
						break;
					default: 
						JOptionPane.showMessageDialog(this, "Client-Default: " + serverMessage);
						break;
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
	
	private void askForNewGame(){
		int choose = JOptionPane.showConfirmDialog(this, "Play Again?", "Play Again?", JOptionPane.YES_NO_OPTION);
		logger.info("{}", choose); //YES = 0 | NO = 1
		if(choose == 1){
			close();
		}
		
		ClientMessage clientMessage = new ClientMessage(name, NEW_GAME);
		clientMessage.setRestart(true);
		System.out.println("Client restart: " + clientMessage);
		sendJson(clientMessage);
	}
	
	private void close(){
		ClientMessage clientMessage = new ClientMessage(name, EXIT);
		sendJson(clientMessage);
		System.exit(0);
	}

	private void sendJson(ClientMessage clientMessage){
		try {
			System.out.println("CLIENT SEND: " + clientMessage);
			printWriter.println(objectMapper.writeValueAsString(clientMessage));
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
	}
}