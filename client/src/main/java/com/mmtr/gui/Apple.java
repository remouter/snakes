package com.mmtr.gui;

import com.mmtr.model.Point2D;

import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.Image;

public class Apple {
	private Point2D position;
	private static Image picture;

	public Apple(){
		try{
			picture = ImageIO.read(this.getClass().getResource("/images/apple.png"));
		}
		catch(Exception e){
		}
	}

	public void setPosition(Point2D position) {
		this.position = position;
	}

	
	public void draw(Graphics2D g2d){
		if(position == null){
			return;
		}

		g2d.drawImage(picture, position.getX(), position.getY(), null);
	}
}