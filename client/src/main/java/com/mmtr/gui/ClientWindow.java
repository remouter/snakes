package com.mmtr.gui;

import com.mmtr.model.Point2D;
import com.mmtr.model.Snake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import static com.mmtr.model.SnakeUtils.FIELD_SIZE;
import static com.mmtr.model.SnakeUtils.HEIGHT_MAX;
import static com.mmtr.model.SnakeUtils.WIDTH_MAX;
import static java.awt.Color.BLACK;
import static java.awt.Color.WHITE;
import static java.awt.Font.BOLD;
import static java.awt.Font.SERIF;
import static java.awt.image.BufferedImage.TYPE_BYTE_GRAY;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;

public class ClientWindow extends JPanel {
	private Logger logger = LoggerFactory.getLogger(ClientWindow.class);
	private static final Integer WIDTH = WIDTH_MAX;
	private static final Integer HEIGHT = HEIGHT_MAX;
	private static final Integer FIELD = FIELD_SIZE;
	private static final Integer FONT_SIZE = 72;
	private BufferedImage buffer;
	private Graphics2D g2d;
	private Boolean showGrid = true;
	private Snake2D[] snake2DS;
	private Image grassPicture;
	private Apple apple = new Apple();
	private Boolean paused = false;

	
	public void setPaused(Boolean paused){
		this.paused = paused;
		repaint();
	}

	public void setSnake(Snake snake){
		snake2DS[0].setSnake(snake);
		repaint();
	}

	public void setApple(Point2D apple){
		this.apple.setPosition(apple);
		repaint();
	}
	
	public ClientWindow(){
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		buffer = new BufferedImage(WIDTH, HEIGHT, TYPE_INT_RGB);
		g2d = buffer.createGraphics();
		snake2DS = new Snake2D[2];

		try{		
			grassPicture = ImageIO.read(this.getClass().getResource("/images/grass.jpg"));
			snake2DS[0] = new Snake2D("red");
			snake2DS[1] = new Snake2D("blue");
		}
		catch(Exception e){
			logger.error(e.getMessage());
		}
	}
	
	public void paint(Graphics g){
		//Draw Grass
		if(paused){
			buffer = new BufferedImage(WIDTH, HEIGHT, TYPE_BYTE_GRAY);
			g2d = buffer.createGraphics();
		}
		else{
			buffer = new BufferedImage(WIDTH, HEIGHT, TYPE_INT_RGB);
			g2d = buffer.createGraphics();
		}
		
		g2d.drawImage(grassPicture, 0, 0, null);
		apple.draw(g2d);
		snake2DS[0].draw(g2d);
		snake2DS[1].draw(g2d);
	
		if(showGrid) drawGrid();
		
		if(paused){
			g2d.setColor(WHITE);
			g2d.setFont(new Font(SERIF, BOLD, FONT_SIZE));
			g2d.drawString("PAUSE", 100, 100);
		}		
		g.drawImage(buffer, 0, 0, this);
	}
	
	private void drawGrid(){
		g2d.setColor(BLACK);
		for(int x = FIELD; x < WIDTH; x += FIELD){
			g2d.drawLine(x, 0, x, HEIGHT);
		}

		for(int y = FIELD; y < HEIGHT; y += FIELD){
			g2d.drawLine(0, y, WIDTH, y);
		}
	}

	public void setShowGrid(Boolean showGrid) {
		this.showGrid = showGrid;
	}

	public Boolean getShowGrid() {
		return showGrid;
	}
}