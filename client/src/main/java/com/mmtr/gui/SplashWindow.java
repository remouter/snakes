package com.mmtr.gui;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.net.URL;

import static javax.swing.SwingConstants.CENTER;

public class SplashWindow {
	private static final Integer DELAY = 2000;
	private static final Integer IMAGE_W = 300;
	private static final Integer IMAGE_H = 200;

	public SplashWindow(String str) throws InterruptedException {
		JWindow window = new JWindow();
		URL url = this.getClass().getResource(str);
		ImageIcon imageIcon = new ImageIcon(url);
		window.add(new JLabel("", imageIcon, CENTER));
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		window.setBounds((screenSize.width - IMAGE_W) / 2, (screenSize.height - IMAGE_H) / 2, IMAGE_W, IMAGE_H);
		window.setVisible(true);

		Thread.sleep(DELAY);

		window.setVisible(false);
		window.dispose();
	}
}