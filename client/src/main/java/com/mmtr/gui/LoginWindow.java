package com.mmtr.gui;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class LoginWindow extends JDialog {
	private final String DEFAULT = "Default";
	private final String LOCALHOST = "localhost";
	private final String PORT_NUMBER = "64444";
	private final String NAME = "Name";
	private final String HOST = "Host";
	private final String PORT = "Port";


	private static final Long serialVersionUID = 1L;
	private static final Integer DEFAULT_WIDTH = 120;
	private static final Integer DEFAULT_HEIGHT = 220;
	private JTextField nameField;
	private JTextField hostField;
	private JTextField portField;

	private Properties properties = new Properties();
	private String name;
	private String host;
	private String port;

	public LoginWindow(Frame owner, Boolean modal){
		super(owner, modal);
		try(FileInputStream fileInputStream = new FileInputStream(
				ClassLoader.getSystemClassLoader()
						.getResource("client.properties")
						.getFile())){
			properties.load(fileInputStream);
			
			name = properties.getProperty(NAME, DEFAULT);
			host = properties.getProperty(HOST, LOCALHOST);
			port = properties.getProperty(PORT, PORT_NUMBER);
		} catch(Exception e){
			name = DEFAULT;
			host = LOCALHOST;
			port = PORT_NUMBER;
		}
		
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((dimension.width / 2  - DEFAULT_WIDTH), (dimension.height / 2 - DEFAULT_HEIGHT));
		setTitle("LoginWindow");
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		setLayout(new FlowLayout());
		JLabel nameLabel = new JLabel("Player: ");
		nameField = new JTextField(10);
		nameField.setText(name);
		add(nameLabel);
		add(nameField);
		
		JLabel hostLabel = new JLabel(HOST);
		hostField = new JTextField(10);
		hostField.setText(host);
		add(hostLabel);
		add(hostField);
		
		JLabel portLabel = new JLabel(PORT);
		portField = new JTextField(10);
		portField.setText(port);
		add(portLabel);
		add(portField);
		
		JButton button = new JButton("Connect");
		
		getRootPane().setDefaultButton(button);

		button.addActionListener(e -> setVisible(false));
		add(button);				
		
		setResizable(false);
		setVisible(true);
		pack();
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent we){
				saveProperties();
			}
		});
	}
	
	public void saveProperties(){
		properties.setProperty(NAME, nameField.getText());
		properties.setProperty(HOST, hostField.getText());
		properties.setProperty(PORT, portField.getText());
		try(FileOutputStream fileOutputStream = new FileOutputStream(ClassLoader.getSystemClassLoader()
				.getResource("client.properties")
				.getFile())){
			properties.store(fileOutputStream, "TEST");
		}
		catch(Exception e){
		}
	}
	
	public String getName(){
		return nameField.getText();
	}

	public String getHost(){
		return hostField.getText();
	}

	public Integer getPort(){
		return Integer.parseInt(portField.getText());
	}
}