package com.mmtr.gui;

import com.mmtr.model.Point2D;
import com.mmtr.model.Snake;

import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.List;

public class Snake2D {
	private Image picture;
	private Image head_img;
	private Image head_left;
	private Image head_right;
	private Image head_top;
	private Image head_down;

	private Point2D head;
	private List<Point2D> body;

	public Snake2D(String color){
		try{
			String path = "/images/" + color;
			this.picture = ImageIO.read(this.getClass().getResource( path + ".png"));
			this.head_img = ImageIO.read(this.getClass().getResource(path + "_down.png"));
			this.head_left = ImageIO.read(this.getClass().getResource(path + "_left.png"));
			this.head_right = ImageIO.read(this.getClass().getResource(path + "_right.png"));
			this.head_top = ImageIO.read(this.getClass().getResource(path + "_top.png"));
			this.head_down = ImageIO.read(this.getClass().getResource(path + "_down.png"));
		}
		catch(Exception e){
		}
	}
	
	public void setSnake(Snake snake){
		try{
			switch(snake.getMovement()){
				case LEFT:
					head_img = head_left;
					break;
				case RIGHT:
					head_img = head_right;
					break;
				case UP:
					head_img = head_top;
					break;
				case DOWN:
					head_img = head_down;
					break;
				default:
					break;
			}			
		}
		catch(Exception e){
		}
		body = snake.getBody();
		head = snake.getHead();
	}	
	
	public void draw(Graphics2D g2d){
		if(head == null){
			return;
		}

		g2d.drawImage(head_img, head.getX(), head.getY(), null);
		body.forEach(p -> g2d.drawImage(picture, p.getX(), p.getY(), null));
	}
}