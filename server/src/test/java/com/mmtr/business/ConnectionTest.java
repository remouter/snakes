package com.mmtr.business;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmtr.model.ClientMessage;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConnectionTest {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void test() throws JsonProcessingException {
        String line = "{\"name\":\"Alex\",\"actionCode\":\"LOGIN\",\"parameter\":null}";
        ClientMessage clientMessage = objectMapper.readValue(line, ClientMessage.class);
        System.out.println(clientMessage);
    }
}