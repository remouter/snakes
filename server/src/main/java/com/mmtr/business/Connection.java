package com.mmtr.business;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmtr.model.ActionCode;
import com.mmtr.model.ClientMessage;
import com.mmtr.model.ServerMessage;
import com.mmtr.Server;
import com.mmtr.model.Snake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.JOptionPane;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static com.mmtr.model.ActionCode.ACCEPT;
import static com.mmtr.model.ActionCode.REJECT;
import static com.mmtr.model.ActionCode.SERVER_UNAVAILABLE;

public class Connection implements Runnable{
	private Logger logger = LoggerFactory.getLogger(Connection.class);

	private Server server;
	private Game game;
	private BufferedReader bufferedReader;
	private PrintWriter printWriter;
	private Socket socket;
	private ObjectMapper objectMapper = new ObjectMapper();
	
	public Connection(Server server, Socket socket, Game game){
		this.server = server;
		this.socket = socket;
		this.game = game;
		new Thread(this).start();
	}
	
	public void run(){
		try{
			bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			printWriter = new PrintWriter(socket.getOutputStream(), true);
			if(game == null){
				ServerMessage serverMessage = new ServerMessage(SERVER_UNAVAILABLE);
				sendJson(serverMessage);
				System.exit(0);
			}

			while(true){
				String line = bufferedReader.readLine();
				if(line != null){
					server.log(line);
				}

				ClientMessage clientMessage = objectMapper.readValue(line, ClientMessage.class);
				System.out.println("ClientMessage: " + clientMessage);
				ActionCode command = clientMessage.getActionCode();
				
				switch(command){						
					case EXIT:
						game.removePlayer();
						break;
					case NEW_GAME:
//						game.restart(clientMessage.getName(), clientMessage.getKey());
						System.out.println("Server NEW_GAME: " + clientMessage);
						game.restart(clientMessage.getName(), clientMessage.isRestart());
						break;
					case LOGIN:
						System.out.println("SERVER LOGIN: " + clientMessage);
						String name = clientMessage.getName();

						ServerMessage serverMessage;
						if(game.isNameAvailable(name)){
							System.out.println("NAME IS AVAILABLE");
							serverMessage = new ServerMessage(ACCEPT);
							Snake snake = game.addPlayer(this, name);
							serverMessage.setSnake(snake);
							serverMessage.setApple(game.getApple());
						}
						else{
							serverMessage = new ServerMessage(REJECT);
						}
						System.out.println("SERVER SEND: " + serverMessage);
						sendJson(serverMessage);
						break;
					case MOVE:
						game.setVelocity(clientMessage.getName(), clientMessage.getKey());
						break;
					case PAUSE:
						game.pause();
						break;
					default: 
						JOptionPane.showMessageDialog(null, "Connection-default: " + clientMessage);
						break;
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	public void sendJson(ServerMessage serverMessage){
		try {
			printWriter.println(objectMapper.writeValueAsString(serverMessage));
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
	}
}