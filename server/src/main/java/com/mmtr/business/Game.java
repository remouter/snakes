package com.mmtr.business;

import com.mmtr.model.Key;
import com.mmtr.model.ServerMessage;
import com.mmtr.model.Point2D;
import com.mmtr.model.Snake;
import com.mmtr.model.SnakeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.util.Random;

import static com.mmtr.model.ActionCode.LOSE;
import static com.mmtr.model.ActionCode.PAUSE_OFF;
import static com.mmtr.model.ActionCode.PAUSE_ON;
import static com.mmtr.model.ActionCode.RESTART;
import static com.mmtr.model.ActionCode.UPDATE;
import static com.mmtr.model.Movement.*;

public class Game implements Runnable{
	private Logger logger = LoggerFactory.getLogger(Game.class);

	private static final Integer FIELD_WIDTH = 50;
	private static final Integer FIELD_HEIGHT = 50;
	private static final Integer FIELD = 50;
	private static Integer GAME_SPEED = 500;
	private static final Integer WIDTH = 800;
	private static final Integer HEIGHT = 600;
	private final Integer POINTS_TO_SPEED_UP = 10;

	private Random random = new Random();
	private Point2D apple;
	private Boolean paused = false;
	private Snake snake;
	private Connection connection;
	
//	private void newAvailablePoints(){
//		for(int i = 0; i < WIDTH / FIELD; i++)
//			for(int j = 0; j < HEIGHT / HEIGHT; j++){
//				availablePoints.add("" + i * FIELD_WIDTH + "," + j * FIELD_HEIGHT);
//			}
//	}

	public void pause(){
		ServerMessage serverMessage;

		if(paused){
			serverMessage = new ServerMessage(PAUSE_OFF);
		} else {
			serverMessage = new ServerMessage(PAUSE_ON);
		}

		paused = !paused;
		connection.sendJson(serverMessage);
	}
	
	public void removePlayer(){
		connection = null;
		snake = null;
}
	
	public void restart(String name, Boolean restart){
		if(restart){
			snake = newSnake(name);
			ServerMessage serverMessage = new ServerMessage(RESTART);
//			Snake refreshSnake = new Snake();
//			snake = refreshSnake;
			serverMessage.setSnake(snake);
			System.out.println("Server RESTART: " + serverMessage);
			connection.sendJson(serverMessage);
		}
	}
	
//	public boolean isHeadCollision(){
//		if(availablePlayers == 1){
//			return false;
//		}
//
//		if(snakes.get(0).getHead().equals(snakes.get(1))){
//			return true;
//		}
//		return false;
//	}
	
	public boolean isNameAvailable(String name){
		return snake == null || (!name.isEmpty() && !snake.getName().equals(name));
	}
	
	public Snake addPlayer(Connection connection, String name){
		Snake snake = null;
		if(name != null && connection != null){
			snake = newSnake(name);
			this.snake = snake;
			this.connection = connection;
		}
		return snake;
	}

	private Snake newSnake(String name){
		int x = (random.nextInt(WIDTH / FIELD - 4) + 2) * FIELD_WIDTH;
		int y = (random.nextInt(HEIGHT / FIELD - 4) + 2) * FIELD_HEIGHT;
		return new Snake(new Point2D(x, y), name);
	}

	public Game(){
		addApple();
		new Thread(this).start();
	}

	public Point2D getApple(){
		return apple;
	}
	
	public void setVelocity(String name, Key velocity){
		switch(velocity){
			case A:
				snake.setVelocity(FIELD_HEIGHT * (-1), 0);
				snake.setMovement(LEFT);
				break;
			case D:
				snake.setVelocity(FIELD_WIDTH, 0);
				snake.setMovement(RIGHT);
				break;
			case S:
				snake.setVelocity(0, FIELD_WIDTH);
				snake.setMovement(DOWN);
				break;
			case W:
				snake.setVelocity(0, FIELD_HEIGHT * (-1));
				snake.setMovement(UP);
				break;
			default: 
				break;
		}
	}

	private void addApple(){
		apple = new Point2D((int)(Math.random() * (WIDTH / FIELD - 1)) * FIELD_WIDTH, (int)(Math.random() * (HEIGHT / FIELD - 1)) * FIELD_HEIGHT);
	}
	
	private void gameUpdate(){
		snake.move();

		//APPLE COLLISION
		if(apple != null && SnakeUtils.isCollided(snake.getHead(), apple)){
			int x = apple.getX() - snake.getVelocityX();
			int y = apple.getY() - snake.getVelocityY();
			snake.growUp(new Point2D(x, y));

			//New Apple
			addApple();

			try{
				AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(this.getClass().getResource("/music/chompSound.wav"));
				Clip chompSound = AudioSystem.getClip();
				chompSound.open(audioInputStream);
				chompSound.start();
			}
			catch(Exception e){
				logger.error(e.getMessage());
			}
		}


//		//Snakes SelfCollided & isBorderCollided Check
		if((SnakeUtils.isSelfCollided(snake)|| SnakeUtils.isBorderCollided(snake.getHead()))){
			ServerMessage serverMessage = new ServerMessage(LOSE);
			connection.sendJson(serverMessage);
			return;
		}

//		//SNAKES COLLISION
//		if(availablePlayers == 2){
//			boolean snake1Col = false, snake2Col = false;
//			if(snakes.get(names[0]).isSnakesCollided(snakes.get(names[1]))){
//				snake1Col = true;
//			}
//
//			if(snakes.get(names[1]).isSnakesCollided(snakes.get(names[0]))){
//				snake2Col = true;
//			}
//
//			ServerMessage serverMessageTest = new ServerMessage(UPDATE);
//			serverMessageTest.setApple(getApple());
//			for(Snake snake : snakes.values()){
//				serverMessageTest.setSnake(snake);
//			}
//
//			System.out.println("Server: " + serverMessageTest);
//			connections.values().forEach(c -> c.sendJson(serverMessageTest));
//
//			if(snake1Col && snake2Col){
//				ServerMessage serverMessage = new ServerMessage(DRAW);
//				connections.get(names[0]).sendJson(serverMessage);
//				connections.get(names[1]).sendJson(serverMessage);
//				names[0] = null;
//				names[1] = null;
//				return;
//			}
//			if(snake1Col){
//				ServerMessage serverMessage = new ServerMessage(LOSE);
//				connections.get(names[0]).sendJson(serverMessage);
//				serverMessage = new ServerMessage(WIN);
//				connections.get(names[1]).sendJson(serverMessage);
//				names[0] = null;
//				names[1] = null;
//				return;
//			}
//			if(snake2Col){
//				ServerMessage serverMessage = new ServerMessage(LOSE);
//				connections.get(names[1]).sendJson(serverMessage);
//				serverMessage = new ServerMessage(WIN);
//				connections.get(names[0]).sendJson(serverMessage);
//				names[0] = null;
//				names[1] = null;
//				return;
//			}
//		}
	}
	
	public void run(){
		while(true){
			try{
				Thread.sleep(GAME_SPEED);
				if(snake != null && !paused){
					gameUpdate();				

					if(snake.getBody().size() == POINTS_TO_SPEED_UP){
						speedUp();
					}
					
					//Send Update
					ServerMessage serverMessage = new ServerMessage(UPDATE);
					serverMessage.setApple(apple);
					serverMessage.setSnake(snake);
					connection.sendJson(serverMessage);
				}
			}
			catch(InterruptedException e){
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	private void speedUp(){
		if(GAME_SPEED > 200) {
			GAME_SPEED -= 50;
		}
	}

	public void setSnake(Snake snake) {
		this.snake = snake;
	}

	public Snake getSnake() {
		return snake;
	}
}