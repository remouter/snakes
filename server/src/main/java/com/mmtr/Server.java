package com.mmtr;

import com.mmtr.business.Connection;
import com.mmtr.business.Game;
import com.mmtr.model.ServerMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import java.awt.Font;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import static com.mmtr.model.ActionCode.SERVER_STOP;
import static java.awt.Color.BLACK;
import static java.awt.Color.WHITE;
import static java.awt.Font.BOLD;
import static java.awt.Font.SERIF;
import static java.awt.Label.CENTER;
import static javax.swing.SpringLayout.NORTH;
import static javax.swing.SpringLayout.SOUTH;

public class Server extends JFrame implements Runnable{
	private Logger logger = LoggerFactory.getLogger(Server.class);

	private String pattern = "yyyy/MM/dd hh:mm:ss.SSS";
	private final String START = "Start";
	private final String STOP = "Stop";
	private final String SERVER_CREATED = "Server created";
	private final String SERVER_STARTED = "Server started";
	private final String SERVER_STOPPED = "Server stopped";
	private final String CLIENT_CONNECTED = "Client connected";
	private final String SERVER = "Server";

	private static final Long serialVersionUID = 1L;
	private static final Integer PORT = 64444;
	private ServerSocket serverSocket;
	private JTextArea textArea;
	private JButton button = new JButton(START);
	private Boolean serverRunning = false;
	private Game game;
	private Thread thread;
	private ArrayList<Connection> connections = new ArrayList<>();
	private Integer fontSize = 32;
	private Integer width = 20;
	private Integer height = 25;

	public Server(){
		init();
		
		thread = new Thread(this);
		log(SERVER_CREATED);
		thread.start();
	}
	
	private void init(){
		JLabel label = new JLabel(SERVER);
		label.setForeground(WHITE);
		label.setBackground(BLACK);
		label.setOpaque(true);
		label.setFont(new Font(SERIF, BOLD, fontSize));
		JPanel topPanel = new JPanel();
		topPanel.setBackground(BLACK);
		topPanel.add(label);
		add(topPanel, NORTH);
		
		textArea = new JTextArea(width, height);
		textArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(textArea);
		add(scrollPane, CENTER);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(button);
		add(buttonPanel, SOUTH);
		
		button.addActionListener(e -> {
			serverRunning = !serverRunning;
			if(serverRunning){
				log(SERVER_STARTED);
				button.setText(STOP);
				game = new Game();
			}
			else{
				log(SERVER_STOPPED);
				button.setText(START);
				thread = null;
				game = null;
				connections.forEach(c -> c.sendJson(new ServerMessage(SERVER_STOP)));
			}
		});
		
		pack();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
	}
	
	public static void main(String[] args){
		SwingUtilities.invokeLater(() -> new Server());
	}
	
	public void run(){
		try{
			serverSocket = new ServerSocket(PORT);
			while(thread != null){
				Socket socket = serverSocket.accept();
				Connection connection = new Connection(this, socket, game);
				connections.add(connection);
				log(CLIENT_CONNECTED);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
	
	public void log(String string){
		LocalDateTime time = LocalDateTime.now();
		String message = String.format("%s: %s\n", time.format(DateTimeFormatter.ofPattern(pattern)), string);
		textArea.append(message);
		logger.info(message);
	}
}