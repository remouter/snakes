package com.mmtr.model;

public enum Movement {
    UP, DOWN, LEFT, RIGHT
}
