package com.mmtr.model;

public class Point2D {
	private Integer x;
	private Integer y;

	public Point2D() {
	}

	public Point2D(Integer x, Integer y){
		this.x = x;
		this.y = y;
	}
	
	public String toString(){
		return String.format("%s,%s", x, y);
	}
	
	public void setX(Integer x){
		this.x = x;
	}

	public void setY(Integer y){
		this.y = y;
	}

	public int getX(){
		return x;
	}

	public int getY(){
		return y;
	}
	
	public Boolean equals(Point2D point){
		return point.x.equals(x) && point.y.equals(y);
	}
}