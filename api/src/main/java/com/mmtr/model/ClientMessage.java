package com.mmtr.model;

public class ClientMessage {
	private String name;
	private ActionCode actionCode;
	private boolean restart;
	private Key key;

	public ClientMessage() {
	}

	public ClientMessage(String name, ActionCode actionCode){
		this.name = name;
		this.actionCode = actionCode;
	}

	public String getName() {
		return name;
	}

	public ActionCode getActionCode() {
		return actionCode;
	}

	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}

	public boolean isRestart() {
		return restart;
	}

	public void setRestart(boolean restart) {
		this.restart = restart;
	}

	@Override
	public String toString() {
		return "ClientMessage{" +
				"name='" + name + '\'' +
				", actionCode=" + actionCode +
				", restart=" + restart +
				", key=" + key +
				'}';
	}
}