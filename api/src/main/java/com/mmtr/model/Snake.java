package com.mmtr.model;

import java.util.ArrayList;
import java.util.Random;

public class Snake {
	private static Random random = new Random();
	private static final Integer VELOCITY = 50;
	private Integer velocityX;
	private Integer velocityY;
	private String name = "Player";

	private ArrayList<Point2D> body = new ArrayList<>();
	private Point2D head;
	private Movement movement;

	public Snake(Point2D head, String name){
		this.head = head;
		this.name = name;
		initVelocity();
	}

	public Snake() {
	}

	public Movement getMovement() {
		return movement;
	}

	public void setMovement(Movement movement) {
		this.movement = movement;
	}

	public Point2D getHead(){
		return head;
	}

	public Integer getVelocityX() {
		return velocityX;
	}

	public Integer getVelocityY() {
		return velocityY;
	}

	public void setPosition(Integer posX, Integer posY){
		body = new ArrayList<>();
		body.add(new Point2D(posX, posY));
	}

	public void initVelocity(){
		this.velocityX  = (random.nextInt(2) - 1) * VELOCITY;
		if(Math.abs(this.velocityX) == VELOCITY){
			this.velocityY = 0;
		} else if(random.nextInt(2) % 2 == 0){
			this.velocityY = VELOCITY * (-1);
		} else{
			this.velocityY = VELOCITY;
		}
	}

	public void setVelocity(Integer x, Integer y){
		velocityX = x;
		velocityY = y;
	}
	
	public void move(){
		if(body.size() != 0){
			body.add(0, new Point2D(head.getX(), head.getY()));
			body.remove(body.size() - 1);
		}
		head.setX(head.getX() + velocityX);
		head.setY(head.getY() + velocityY);
	}

	public void growUp(Point2D point){
		body.add(point);
	}
	
	public ArrayList<Point2D> getBody(){
		return body;
	}
	
	public Boolean isSnakesCollided(Snake other){
		Point2D head = body.get(0);
		for(Point2D point : other.getBody()){
			if(head.equals(point)){
				return true;
			}
		}
		return false;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Snake{" +
				"velocityX=" + velocityX +
				", velocityY=" + velocityY +
				", name='" + name + '\'' +
				", body=" + body +
				", head=" + head +
				", movement=" + movement +
				'}';
	}
}