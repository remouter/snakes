package com.mmtr.model;

public class SnakeUtils {
    public static final Integer FIELD_SIZE =  50;
    public static final Integer WIDTH_MAX  =  800;
    public static Integer HEIGHT_MAX  = 600;

    public static Boolean isBorderCollided(Point2D head){
        if(head.getX() < 0 || head.getX() > WIDTH_MAX - FIELD_SIZE||
                head.getY() < 0 || head.getY() > HEIGHT_MAX - FIELD_SIZE){
            return true;
        }
        return false;
    }

    public static boolean isCollided(Point2D head, Point2D apple){
        return head.equals(apple);
    }

    public static Boolean isSelfCollided(Snake snake){
		Point2D head = snake.getHead();
        for(int i = 0; i < snake.getBody().size(); i++){
            if(head.equals(snake.getBody().get(i))){
                return true;
            }
        }
        return false;
    }
}
