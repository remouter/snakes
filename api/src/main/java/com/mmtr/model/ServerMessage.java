package com.mmtr.model;

public class ServerMessage {
	private Point2D apple;
	private ActionCode actionCode;
	private Snake snake;

	public ServerMessage() {
	}

	public ServerMessage(ActionCode actionCode) {
		this.actionCode = actionCode;
	}

	public ActionCode getActionCode(){
		return actionCode;
	}

	public Point2D getApple() {
		return apple;
	}

	public void setApple(Point2D apple) {
		this.apple = apple;
	}

	public void setSnake(Snake snake) {
		this.snake = snake;
	}

	public Snake getSnake() {
		return snake;
	}

	@Override
	public String toString() {
		return "ServerMessage{" +
				"apple=" + apple +
				", actionCode=" + actionCode +
				", snake=" + snake +
				'}';
	}
}